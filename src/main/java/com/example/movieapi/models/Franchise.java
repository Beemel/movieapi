package com.example.movieapi.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.stream.Collectors;

@Entity
public class Franchise {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Size(max = 50)
    @Column(name = "name" , length = 50, nullable = true)
    private String name;
    @Size(max = 2000)
    @Column(name = "description", columnDefinition = "TEXT", length = 2000, nullable = true)
    private String description;

    @OneToMany
    @JoinColumn(name = "franchise_id")
    List<Movie> movies;

    @JsonGetter ("movie")
    public List<String>movies(){
        if (movies != null){
            return movies.stream().map(movie ->{
                return "/api/v1/movieapi/movie/" + movie.getId();
                }).collect(Collectors.toList());
        }
        return null;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
