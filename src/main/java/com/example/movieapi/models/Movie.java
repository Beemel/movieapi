package com.example.movieapi.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.net.URL;
import java.util.List;
import java.util.stream.Collectors;

@Entity
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotBlank
    @Size(max = 100)
    @Column(name = "title", length = 100, nullable = false)
    private String title;
    @NotBlank
    @Size(max = 100)
    @Column(name = "genre", length = 100, nullable = false)
    private String genre;
    @NotBlank
    @Size(max = 4)
    @Column(name = "release_year", length = 4, nullable = false)
    private int releaseYear;
    @NotBlank
    @Size(max = 50)
    @Column(name = "director", length = 50, nullable = false)
    private String director;
    @Size(max = 200)
    @Column(name = "picture", length = 200, nullable = true)
    private URL picture;
    @Size(max = 200)
    @Column(name = "trailer", length = 200, nullable = true)
    private URL trailer;

    @ManyToMany
    @JoinTable (
            name = "movie_character",
            joinColumns = {@JoinColumn(name = "movie_id")},
            inverseJoinColumns = {@JoinColumn(name = "character_id")}    )
    public List<Character> characters;

    @JsonGetter ("characters")
    public List<String> characters() {
        if (characters != null) {
            return characters.stream().distinct()
                    .map(character -> {
                        return "/api/v1/movieapi/character/" + character.getId();
                    }).collect(Collectors.toList());
        }
        return null;
    }

    @ManyToOne ()
    @JoinColumn(name = "franchise_id")
    public Franchise franchise;

    @JsonGetter ("franchise")
    public String franchise(){
        if (franchise!= null){
            return "/api/v1/movieapi/franchise/"+ franchise.getId();
        }else{
            return null;
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(int releaseYear) {
        this.releaseYear = releaseYear;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public URL getPicture() {
        return picture;
    }

    public void setPicture(URL picture) {
        this.picture = picture;
    }

    public URL getTrailer() {
        return trailer;
    }

    public void setTrailer(URL trailer) {
        this.trailer = trailer;
    }
}
