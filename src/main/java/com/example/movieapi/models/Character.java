package com.example.movieapi.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.net.URL;
import java.util.List;
import java.util.stream.Collectors;

@Entity
public class Character {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Size(max = 50)
    @Column(name = "full_name", length = 50, nullable = false)
    private String fullName;
    @NotBlank
    @Size(max = 200)
    @Column(name = "alias", length = 200, nullable = false)
    private String alias;
    @Size(max = 25)
    @Column(name = "gender", length = 25, nullable = true)
    private String gender;
    @NotBlank
    @Size(max = 200)
    @Column(name = "picture", length = 200, nullable = false)
    private URL picture;

    @ManyToMany
    @JoinTable (
            name = "movie_character",
            joinColumns = {@JoinColumn(name = "character_id")},
            inverseJoinColumns = {@JoinColumn(name = "movie_id")}    )
    public List<Movie> movies;

    @JsonGetter("movies")
    public List<String> movies() {
        return movies.stream().map(movie -> {
            return "/api/v1/movieapi/movie/" + movie.getId();
        }).collect(Collectors.toList());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public URL getPicture() {
        return picture;
    }

    public void setPicture(URL picture) {
        this.picture = picture;
    }
}
