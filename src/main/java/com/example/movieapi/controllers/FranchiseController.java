package com.example.movieapi.controllers;
import com.example.movieapi.models.Franchise;
import com.example.movieapi.repositories.FranchiseRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/movieapi/franchise")
@Tag(name = "Franchises", description = "This controls the operations for franchises")
public class FranchiseController {
    @Value("${spring.datasource.url}")
    private String url;
    private Connection con = null;
    @Autowired
    private FranchiseRepository franchiseRepository;

    @Operation(summary = "Gets all the franchises")
    @GetMapping
    public ResponseEntity<List<Franchise>> getAllFranchises() {
        List<Franchise> franchises = franchiseRepository.findAll();
        return new ResponseEntity<>(franchises, HttpStatus.OK);
    }

    @Operation(summary = "Gets a specific franchise by its unique id")
    @GetMapping("/{id}")
    public ResponseEntity<Franchise> getFranchise(@PathVariable Long id) {
        Franchise returnFranchise = new Franchise();
        if (franchiseRepository.existsById(id)) {
            returnFranchise = franchiseRepository.findById(id).get();
            return new ResponseEntity<>(returnFranchise, HttpStatus.OK);
        }
        return new ResponseEntity<>(returnFranchise, HttpStatus.NOT_FOUND);
    }

    @Operation(summary = "Adds a new franchise if you follow the template for franchise")
    @PostMapping
    public ResponseEntity<Franchise> addFranchise(@RequestBody Franchise franchise) {
        franchise = franchiseRepository.save(franchise);
        return new ResponseEntity<>(franchise, HttpStatus.CREATED);
    }

    @Operation(summary = "Updates a franchise if you follow the template for franchise and the id exists")
    @PutMapping("/{id}")
    public ResponseEntity<Franchise> updateCharacter(@PathVariable Long id, @RequestBody Franchise franchise) {
        Franchise returnFranchise = new Franchise();
        if (!id.equals(franchise.getId())) {
            return new ResponseEntity<>(returnFranchise, HttpStatus.NOT_FOUND);
        }
        returnFranchise = franchiseRepository.save(franchise);
        return new ResponseEntity<>(returnFranchise, HttpStatus.NO_CONTENT);
    }

    //The delete function is hidden in swagger to not accidentally being able to delete a franchise,
    // the method can be done in program like Postman.
    @Operation(summary = "Deletes a franchise if the id exists", hidden = true)
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteFranchise(@PathVariable Long id) {
        if (!franchiseRepository.existsById(id)) {
            return new ResponseEntity<>("Franchise with id " + id + " does not exists!", HttpStatus.NOT_FOUND);
        }
        franchiseRepository.deleteById(id);
        return new ResponseEntity<>("Franchise with id " + id + " has been deleted!", HttpStatus.OK);
    }

    @Operation(summary = "Get all movies in a franchise")
    @GetMapping("/{id}/movies")
    public ResponseEntity<List> getMoviesFromFranchise(@PathVariable Long id) {
        List<String> movieList = new ArrayList<>();
        StringBuilder sql = new StringBuilder(3);
        sql.append("SELECT movie.id FROM movie");
        sql.append(" INNER JOIN franchise ON franchise.id = movie.franchise_id");
        sql.append(" WHERE franchise.id = ?");

        try { con = DriverManager.getConnection(url);
            PreparedStatement ps = con.prepareStatement(sql.toString());
            ps.setLong(1, id);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    movieList.add(rs.getString("id"));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        List<String> returnMessage = movieList
                .stream()
                .distinct()
                .toList();

        return new ResponseEntity<>(returnMessage
                .stream()
                .map(returnMess -> "/api/v1/movieapi/movie/" + returnMess)
                .collect(Collectors.toList())
                , HttpStatus.OK);
    }

    @Operation(summary = "Get all character in a franchise")
    @GetMapping("/{id}/characters")
    public ResponseEntity<List> getCharactersFromFranchise(@PathVariable Long id) {
        List<String> characterList = new ArrayList<>();
        StringBuilder sql = new StringBuilder(5);
        sql.append("SELECT character.id FROM character");
        sql.append(" INNER JOIN movie_character ON movie_character.character_id = character.id");
        sql.append(" INNER JOIN movie ON movie.id = movie_character.movie_id");
        sql.append(" INNER JOIN franchise ON franchise.id = movie.franchise_id");
        sql.append(" WHERE franchise.id = ?");

        try { con = DriverManager.getConnection(url);
            PreparedStatement ps = con.prepareStatement(sql.toString());
            ps.setLong(1, id);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    characterList.add(rs.getString("id"));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        List<String> returnMessage = characterList
                .stream()
                .distinct()
                .toList();

        return new ResponseEntity<>(returnMessage
                .stream()
                .map(returnMess -> "/api/v1/movieapi/character/" + returnMess)
                .collect(Collectors.toList())
                , HttpStatus.OK);
    }

    @Operation(summary = "Update movies in a franchise")
    @PutMapping("{id}/updatemovies")
    public ResponseEntity<List> updateMoviesInFranchise(@PathVariable int id, @RequestBody int[] moviesToAdd) {
        List<String> characterList = new ArrayList<>();
        for (int i = 0; i < moviesToAdd.length; i++) {
            try {
                con = DriverManager.getConnection(url);
                PreparedStatement ps = con.prepareStatement("UPDATE movie SET franchise_id = ? WHERE movie.id= ?");
                    ps.setInt(1, id);
                    ps.setInt(2, moviesToAdd[i]);
                    ps.executeUpdate();
            }
            catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return new ResponseEntity<>(characterList, HttpStatus.OK);
    }
}