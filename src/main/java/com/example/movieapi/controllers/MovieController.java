package com.example.movieapi.controllers;

import com.example.movieapi.models.Movie;
import com.example.movieapi.repositories.MovieRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/movieapi/movie")
@Tag(name = "Movie", description = "This controls the operations for movies")
public class MovieController {
    @Value("${spring.datasource.url}")
    private String url;
    private Connection con = null;
    @Autowired
    private MovieRepository movieRepository;

    @Operation(summary = "Gets all the movies")
    @GetMapping
    public ResponseEntity<List<Movie>> getAllMovies() {
        List<Movie> movies = movieRepository.findAll();
        return new ResponseEntity<>(movies, HttpStatus.OK);
    }
    @Operation(summary = "Gets a specific movie by its unique id")
    @GetMapping("/{id}")
    public ResponseEntity<Movie> getMovie(@PathVariable Long id) {
        Movie returnMovie = new Movie();
        if (movieRepository.existsById(id)) {
            returnMovie = movieRepository.findById(id).get();
            return new ResponseEntity<>(returnMovie, HttpStatus.OK);
        }
            return new ResponseEntity<>(returnMovie, HttpStatus.NOT_FOUND);
    }


    @Operation(summary = "Adds a new movie if you follow the template for movies")
    @PostMapping
    public ResponseEntity<Movie> addMovie(@RequestBody Movie movie) {
        movie = movieRepository.save(movie);
        return new ResponseEntity<>(movie, HttpStatus.CREATED);
    }

    @Operation(summary = "Updates a movie if you follow the template for movies and the id exists")
    @PutMapping("/{id}")
    public ResponseEntity<Movie> updateMovie(@PathVariable Long id, @RequestBody Movie movie) {
        Movie returnMovie = new Movie();
        if (!id.equals(movie.getId())) {
            return new ResponseEntity<>(returnMovie, HttpStatus.NOT_FOUND);
        }
        returnMovie = movieRepository.save(movie);
        return new ResponseEntity<>(returnMovie, HttpStatus.NO_CONTENT);
    }

    //The delete function is hidden in swagger to not accidentally being able to delete a movie,
    // the method can be done in program like Postman.
    @Operation(summary = "Deletes a movie if the id exists", hidden = true)
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteMovie(@PathVariable Long id) {
        if (!movieRepository.existsById(id)) {
            return new ResponseEntity<>("Movie with id " + id + " does not exists!", HttpStatus.NOT_FOUND);
        }
        movieRepository.deleteById(id);
        return new ResponseEntity<>("Movie with id " + id + " has been deleted!", HttpStatus.OK);
    }

    @Operation (summary= "Get all character in a movie")
    @GetMapping ("/{id}/characters")
    public ResponseEntity<List> getCharactersFromMovie(@PathVariable Long id) {
        List<String> characterList = new ArrayList<>();
        StringBuilder sql = new StringBuilder(3);
        sql.append("SELECT character.id FROM character");
        sql.append(" INNER JOIN movie_character ON movie_character.character_id = character.id");
        sql.append(" WHERE movie_character.movie_id= ?");

        try{ con = DriverManager.getConnection(url);
            PreparedStatement ps = con.prepareStatement(sql.toString());
            ps.setLong(1, id);
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()){
                    characterList.add (rs.getString("id"));
                }
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }

        List <String> returnMessage = characterList
                .stream()
                .distinct()
                .toList();

        return new ResponseEntity<> (returnMessage
                .stream()
                .map(returnMess -> "/api/v1/movieapi/character/" + returnMess)
                .collect(Collectors.toList())
                , HttpStatus.OK);
    }

    @Operation(summary = "Update characters in a movie")
    @PutMapping("{id}/updatecharacters")
    public ResponseEntity<String> updateCharactersInMovie(@PathVariable Long id, @RequestBody int[] charactersToAdd) {
        StringBuilder sql = new StringBuilder(2);
        sql.append("INSERT INTO movie_character (movie_id, character_id)");
        sql.append(" VALUES (?, ?)");

        for (int i = 0; i < charactersToAdd.length; i++) {
            try{
                con = DriverManager.getConnection(url);
                PreparedStatement ps = con.prepareStatement(sql.toString());
                ps.setLong(1, id);
                ps.setInt(2, charactersToAdd[i]);
                ps.executeUpdate();
            }
            catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return new ResponseEntity<>("characters added", HttpStatus.OK);
    }
}
