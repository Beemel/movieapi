package com.example.movieapi.controllers;

import com.example.movieapi.models.Character;
import com.example.movieapi.repositories.CharacterRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/movieapi/character")
@Tag(name = "Characters", description = "This controls the operations for characters")
public class CharacterController {

    @Autowired
    private CharacterRepository characterRepository;

    @Operation(summary = "Gets all the characters")
    @GetMapping
    public ResponseEntity<List<Character>> getAllCharacters() {
        List<Character> characters = characterRepository.findAll();
        return new ResponseEntity<>(characters, HttpStatus.OK);
    }

    @Operation(summary = "Gets a specific character by its unique id")
    @GetMapping("/{id}")
    public ResponseEntity<Character> getCharacter(@PathVariable Long id) {
        Character returnCharacter = new Character();
        if (characterRepository.existsById(id)) {
            returnCharacter = characterRepository.findById(id).get();
            return new ResponseEntity<>(returnCharacter, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(returnCharacter, HttpStatus.NOT_FOUND);
    }

    @Operation(summary = "Creates a new character if you follow the template for a character")
    @PostMapping
    public ResponseEntity<Character> addCharacter(@RequestBody Character character) {
        character = characterRepository.save(character);
        return new ResponseEntity<>(character, HttpStatus.CREATED);
    }

    @Operation(summary = "Updates a character if you follow the template for character and the id exists")
    @PutMapping("/{id}")
    public ResponseEntity<Character> updateCharacter(@PathVariable Long id, @RequestBody Character character) {
        Character returnCharacter = new Character();
        if (!id.equals(character.getId())) {
            return new ResponseEntity<>(returnCharacter, HttpStatus.NOT_FOUND);
        }
        returnCharacter = characterRepository.save(character);
        return new ResponseEntity<>(returnCharacter, HttpStatus.NO_CONTENT);
    }

    //The delete function is hidden in swagger to not accidentally being able to delete a character,
    // the method can be done in program like Postman.
    @Operation(summary = "Deletes a character if the id exists", hidden = true)
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteCharacter(@PathVariable Long id) {
        if (!characterRepository.existsById(id)) {
            return new ResponseEntity<>("Character with id " + id + " does not exists!", HttpStatus.NOT_FOUND);
        }
        characterRepository.deleteById(id);
        return new ResponseEntity<>("Character with id " + id + " has been deleted!", HttpStatus.OK);
    }
}

