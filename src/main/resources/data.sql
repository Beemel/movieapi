INSERT INTO character (alias, full_name, gender, picture) VALUES
      ('N/A', 'John McClane', 'Male', 'https://www.imdb.com/title/tt0095016/mediaviewer/rm431651840/'),
      ('N/A', 'Holly Genaro McClane', 'Female', 'https://www.imdb.com/name/nm0000889/mediaviewer/rm1130579712/'),
      ('Don', 'Michael Corleone', 'Male', 'https://www.imdb.com/name/nm0000199/mediaviewer/rm1871194624/'),
      ('Don', 'Vito Corleone', 'Male', 'https://www.imdb.com/name/nm0000008/mediaviewer/rm23063553/'),
      ('Consigliere', 'Tom Hagen', 'Male', 'https://www.imdb.com/name/nm0000380/mediaviewer/rm2175635713/'),
      ('The Terminator, T-800, T-850', 'Cyberdyne Systems Model 101', 'Cybernetic Organism', 'https://www.imdb.com/name/nm0000216/mediaviewer/rm1614682368/'),
      ('N/A', 'Sarah Connor', 'Female', 'https://www.imdb.com/name/nm0000157/mediaviewer/rm3620099072/'),
      ('N/A', 'John Connor', 'Male', 'https://www.imdb.com/name/nm0000411/mediaviewer/rm3410504960/');

INSERT INTO franchise (description, "name") VALUES
    ('Die Hard is an American action film series that originated with Roderick Thorp''s novel Nothing Lasts Forever.' ||
     ' All five films revolve around the main character of John McClane, a New York City/Los Angeles police detective who continually finds himself in the middle of a crisis where he is the only hope against disaster.' ||
     '[3] The films have grossed a combined $1.4 billion worldwide.', 'Die Hard'),
    ('The Godfather trilogy is one of the most acclaimed franchises in film history. The Godfather, released in 1972, is an adaptation of the Puzo novel of the same name. The Godfather Part II,' ||
     ' released in 1974, also adapts elements from the first novel - mostly the early life of Vito Corleone. The story of The Godfather Part III, released in 1990, is not taken from any novel.' ||
     ' The Winegardner novels, released after Part III, incorporate and explain elements from Part II and Part III. Falco''s novel, The Family Corleone,' ||
     ' was based on an unproduced screenplay written by Puzo (intended for a fourth Godfather film, which was abandoned after Puzo''s death).',
     'Godfather trilogy'),
    ('Terminator is an American media franchise created by James Cameron and Gale Anne Hurd. The franchise encompasses a series of science fiction action films, comics, novels, and additional media, concerning a total war between Skynet''s' ||
     ' synthetic intelligence – a self-aware military machine network –' ||
     ' and John Connor''s Resistance forces comprising the survivors of the human race.' ||
     ' Skynet''s most famous products in its genocidal goals are the various terminator models, such as the T-800,' ||
     ' who was portrayed by Arnold Schwarzenegger from the original Terminator film in 1984. By 2010, the franchise had generated $3 billion in revenue.[4]',
     'Terminator');

INSERT INTO movie (franchise_id, director, genre, picture, release_year, title, trailer) VALUES
     (1, 'John McTiernan', 'Action, Thriller', 'https://www.imdb.com/title/tt0095016/mediaviewer/rm270892032/', 1988, 'Die hard', 'https://www.youtube.com/watch?v=jaJuwKCmJbY'),
     (1, 'Renny Harlin', 'Action, Thriller', 'https://www.imdb.com/title/tt0099423/mediaviewer/rm3293919488/', 1990, 'Die hard 2 - Die harder', 'https://www.youtube.com/watch?v=CvHp7xJZ4_U'),
     (1, 'John McTiernan', 'Action, Thriller', 'https://www.imdb.com/title/tt0112864/mediaviewer/rm2983138304/', 1995, 'Die hard 3 - With a vengeance', 'https://www.youtube.com/watch?v=_-EX4X13hYc'),
     (2, 'Francis Ford Coppola', 'Drama, Crime', 'https://www.imdb.com/title/tt0068646/mediaviewer/rm746868224/', 1972, 'The Godfather', 'https://www.youtube.com/watch?v=sY1S34973zA'),
     (2, 'Francis Ford Coppola', 'Drama, Crime', 'https://www.imdb.com/title/tt0071562/mediaviewer/rm4159262464/', 1974, 'The Godfather - Part II', 'https://www.youtube.com/watch?v=9O1Iy9od7-A'),
     (2, 'Francis Ford Coppola', 'Drama, Crime', 'https://www.imdb.com/title/tt0099674/mediaviewer/rm1130949632/', 1990, 'The Godfather - Part III', 'https://www.youtube.com/watch?v=UUkG37KSWf0'),
     (3, 'James Cameron', 'Action, Sci-Fi', 'https://www.imdb.com/title/tt0088247/mediaviewer/rm774208512/', 1984, 'The Terminator', 'https://www.youtube.com/watch?v=k64P4l2Wmeg'),
     (3, 'James Cameron', 'Action, Sci-Fi', 'https://www.imdb.com/title/tt0103064/mediaviewer/rm1982141440/', 1991, 'The Terminator 2 - Judgement day', 'https://www.youtube.com/watch?v=CRRlbK5w8AE'),
     (3, 'Jonathan Mostow', 'Action, Sci-Fi', 'https://www.imdb.com/title/tt0181852/mediaviewer/rm4149382144/', 2003, 'The Terminator 3 - Rise of the machines', 'https://www.youtube.com/watch?v=5UgPJhyJmlM');


INSERT INTO movie_character (movie_id, character_id) VALUES (1, 1);
INSERT INTO movie_character (movie_id, character_id) VALUES (1, 2);
INSERT INTO movie_character (movie_id, character_id) VALUES (2, 1);
INSERT INTO movie_character (movie_id, character_id) VALUES (2, 2);
INSERT INTO movie_character (movie_id, character_id) VALUES (3, 1);
INSERT INTO movie_character (movie_id, character_id) VALUES (4, 3);
INSERT INTO movie_character (movie_id, character_id) VALUES (4, 4);
INSERT INTO movie_character (movie_id, character_id) VALUES (4, 5);
INSERT INTO movie_character (movie_id, character_id) VALUES (5, 3);
INSERT INTO movie_character (movie_id, character_id) VALUES (5, 5);
INSERT INTO movie_character (movie_id, character_id) VALUES (6, 3);
INSERT INTO movie_character (movie_id, character_id) VALUES (7, 6);
INSERT INTO movie_character (movie_id, character_id) VALUES (7, 7);
INSERT INTO movie_character (movie_id, character_id) VALUES (8, 6);
INSERT INTO movie_character (movie_id, character_id) VALUES (8, 7);
INSERT INTO movie_character (movie_id, character_id) VALUES (8, 8);
INSERT INTO movie_character (movie_id, character_id) VALUES (9, 6);
INSERT INTO movie_character (movie_id, character_id) VALUES (9, 8);


