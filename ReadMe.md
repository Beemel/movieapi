# Final Assignment JAVA,

## Movie Database Application

### How it works

This is a Spring Boot App where we simulate the usage of controller methods with the use of Postman
application.

We use Swagger to create our documentation for this project.
<br>The URL to our Swagger documentation is : http://localhost:8080/swagger-ui/index.html

Our database for this project is Postgresql which is called: "Movies".

There is a Postman link collection (Movie API.postman_collection.json) 
in our project root folder that you may import and use.

We have some URLs that you can visit to see the specific lists in your Postman / browser.

#### GET Request the full list of the Movies, Characters and Franchises:
http://localhost:8080/api/v1/movieapi/character
<br>http://localhost:8080/api/v1/movieapi/movie
<br>http://localhost:8080/api/v1/movieapi/franchise

#### GET Request a specific Movie, Character, Franchise by its id:
http://localhost:8080/api/v1/movieapi/character/(id)
<br>http://localhost:8080/api/v1/movieapi/movie/(id)
<br>http://localhost:8080/api/v1/movieapi/franchise/(id)

#### GET Request a list of all the movies in a specific franchise:
http://localhost:8080/api/v1/movieapi/franchise/(id)/movies

#### GET Request a list of all the character in a specific franchise:
http://localhost:8080/api/v1/movieapi/franchise/(id)/characters

#### GET Request a list of all the characters in a specific movie:
http://localhost:8080/api/v1/movieapi/movie/(id)/characters

#### POST request in Postman, Create a new Character, Movie or Franchise:
http://localhost:8080/api/v1/movieapi/character/

http://localhost:8080/api/v1/movieapi/movie/

http://localhost:8080/api/v1/movieapi/franchise/


<br>Creates a new character,movie or franchise but you must follow the template otherwise you will get an exception.

#### Here is an example of the acceptable POST request character JSON body:
```
{
    "fullName": "Name of character",
    "alias": "Some aliases the character may have",
    "gender": "Female/Male",
    "picture": "Link to a picture of the character",
    "movies": []
}
```
#### Here is an example of the acceptable POST request movie JSON body:
```
{
    "title": "Movietitle",
    "genre": "Action, Thriller",
    "releaseYear": 1990,
    "director": "Directorname",
    "picture": "Link to a picture of the movie",
    "trailer": "Link to a trailer for the movie",
    "characters": [],
    "franchise": null
}
```
#### Here is an example of the acceptable POST request franchise JSON body:
```
{
    "name": "Name of the franchise",
    "description": "Some Description about the franchise",
    "movie": []
}
```

#### PUT request in Postman ,Update/Replace a character/movie or franchise:
http://localhost:8080/api/v1/movieapi/character/(id)
<br>http://localhost:8080/api/v1/movieapi/movie/(id)
<br>http://localhost:8080/api/v1/movieapi/franchise/(id)
<br>Targets the specified id that u put in the url and replaces it if you followed the template otherwise an exception will
be made.

#### Here is an example of the acceptable PUT request character JSON body:
```
{
    "id": (id),
    "fullName": "Name of character",
    "alias": "Some aliases the character may have",
    "gender": "Female/Male",
    "picture": "Link to a picture of the character",
    "movies": []
}
```
#### Here is an example of the acceptable PUT request movie JSON body:
```
{
    "id": (id),
    "title": "Movietitle",
    "genre": "Action, Thriller",
    "releaseYear": 1990,
    "director": "Directorname",
    "picture": "Link to a picture of the movie",
    "trailer": "Link to a trailer for the movie",
    "characters": [],
    "franchise": null
}
```
#### Here is an example of the acceptable PUT request franchise JSON body:
```
{
    "id": (id),
    "name": "Name of the franchise",
    "description": "Some Description about the franchise",
    "movie": []
}
```
#### PUT request in Postman ,Update/Replace a movie/movies that belongs in a franchise:
http://localhost:8080/api/v1/movieapi/franchise/(id)/updatemovies
#### Here is an example of the acceptable PUT request JSON body:
```
{
[1, 3, 5]
}
```
#### PUT request in Postman ,Update/Replace character/characters that belongs in a movie:
http://localhost:8080/api/v1/movieapi/character/(id)/updatecharacters
#### Here is an example of the acceptable PUT request JSON body:
```
{
[1, 5]
}
```

#### DELETE request Delete a character, movie or franchise by its id:
http://localhost:8080/api/v1/movieapi/character/(id)
<br>http://localhost:8080/api/v1/movieapi/movie(id)
<br>http://localhost:8080/api/v1/movieapi/franchise/(id)
<br>Deletes the targeted character, movie or franchise with the use of your (id) input in the url if it exists.

### About:
This is our last big assignment in which we were asked to create and API that keep tracks
of Franchises, Movies and characters.
<br>We used Hibernate to create a database (postgresql) in which we keep values and make connections between the tables
so a franchise can have multiple movies, movies can have multiple characters, characters can be in multiple movies
and a movie can only belong to one franchise.
<br>You are able to do basic CRUD operations, get list of movies in franchises, get list of characters in a specific 
movie, get a list of characters that belongs in a specific franchise as described in this document.

<br>//Jerry Lindström
<br>//Morgan Engström